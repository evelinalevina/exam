#include <cart.hpp>

namespace el
{
	Cart::Cart(int x, int y)
	{
		m_x = x;
		m_y = y;
	}

	bool Cart::Setup()
	{
		if (!m_texture.loadFromFile("img/cart.jpg"))
		{
			std::cout << "ERROR when loading cart.jpg" << std::endl;
			return false;
		}

		m_cart = new sf::Sprite();
		m_cart->setTexture(m_texture);
		m_cart->setOrigin(100, 100);
		m_cart->setPosition(m_x, m_y);
		m_cart->setScale(0.8, 0.8);

		return true;
	}

	Cart::~Cart()
	{
		if (m_cart != nullptr)
			delete m_cart;
	}

	void Cart::Move(float mx, float my)
	{
		m_mx = mx;
		m_my = my;
		if (mx < 260) m_mx = 260;
		if (mx > 700) m_mx = 700;
		if (my < 320) m_my = 320;
		if (my > 500) m_my = 500;
		m_cart->setPosition(m_mx, m_my);
	}

	sf::Sprite* Cart::Get() { return m_cart; }



}
