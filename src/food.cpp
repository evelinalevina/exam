#include <food.hpp>
#include <iostream>



namespace el
	{
		Food::Food(int x, int y, float angle, float speed)
		{
			m_x0 = x;
			m_y0 = y;
			m_angle = (360 -angle)/180 * acos(-1);
			m_speed = speed;
		}
	
		Food::~Food()
		{
			if (m_food != nullptr)
				delete m_food;
		}

		bool Food::Setup(std::string foodImage)
		{
			m_foodImage = foodImage;
			if (!m_texture.loadFromFile(foodImage)) //if (!m_texture.loadFromFile("img/milk.jpg"))
			{
				std::cout << "ERROR when loading image" << std::endl;
				return false;
			}

			m_food = new sf::Sprite();
			m_food->setTexture(m_texture);
			m_food->setPosition(m_x0, m_y0);
			

			return true;
		}
		void Food::Move(int x, int y)
		{
			m_x = x;
			m_y = y;
			m_food->setPosition(m_x, m_y);
		}

		sf::Sprite* Food::Get() { return m_food; }
		
	
	}