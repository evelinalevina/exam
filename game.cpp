﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <food.hpp>
#include <iostream>
#include <cart.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std::chrono_literals;

int main()
{
    int x0 = 150, y0 = 420, score = 0, speed0 = 30;
    float milk_t = 0, croissant_t = 0, milk_x, milk_y, croissant_x, croissant_y,
          angle = 50, g = 9.8, milk_speed = speed0, croissant_speed = speed0, cart_x=0, cart_y=0 ;

    std::string milkImage = "img/milk.jpg",
        croissantImage = "img/croissant.jpg";
 
    // Создание окна
    sf::RenderWindow window(sf::VideoMode(800, 600), "Catch food!");

    // Подгрузка фонового изображения
    sf::Texture texture;
    if (!texture.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR when loading back.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);

    // Подгрузка шрифта и создание отображения счета
    sf::Font font;
    if (!font.loadFromFile("fonts/arial.ttf"))
    {
        std::cout << "ERROR: font was not loaded." << std::endl;
        return -1;
    }

    sf::Text text;
    text.setFont(font);
    text.setString("Hello world");
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::Red);
    text.setPosition(640, 30);


    // Добавление иконки
    sf::Image icon;
    if (!icon.loadFromFile("img/cart32.png"))
    {
        return -1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

    // Корзина
    el::Cart* cart = nullptr;
    cart = new el::Cart(480, 410);
    cart->Setup();

    // Молоко
    el::Food* milk = nullptr;
    milk = new el::Food(x0, y0, angle, milk_speed);
    milk->Setup(milkImage);

    // Круассан
    el::Food* croissant = nullptr;
    croissant = new el::Food(x0, y0, angle, milk_speed + 30);
    croissant->Setup(croissantImage);

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // Движение еды
        int randPoint1 = 0, randPoint2 = -130;
        float angle_rad = (360 - angle) / 180 * acos(-1);

        milk_x = x0 + milk_speed * cos(angle_rad) * milk_t;
        milk_y = randPoint1 + y0 + speed0 * sin(angle_rad) * milk_t + g * milk_t * milk_t / 2;

        milk->Move(milk_x, milk_y);
        
        croissant_x = x0 + croissant_speed * cos(angle_rad) * croissant_t;
        croissant_y = randPoint2 + y0 + speed0 * sin(angle_rad) * croissant_t + g * croissant_t * croissant_t / 2;

        croissant->Move(croissant_x, croissant_y);

        milk_t += 0.2;
        croissant_t += 0.2;
    
        if ((randPoint1>=0)&&(milk_y>=510) || (randPoint1 <= -130) && (milk_y >= 400)) 
        {
            milk_t = 0;
            randPoint1 = ((rand() % 2) * -130);
            milk_x = x0;
            milk_y = y0 + randPoint1;
            milk_speed = speed0 + ((rand() % 90));
        }

        if ((randPoint2 >= 0) && (croissant_y >= 510) || (randPoint2 <= -130) && (croissant_y >= 400)) 
        {
            croissant_t = 0;
            if (randPoint1 == 0) randPoint2 = -130; else randPoint2 = 0;
            croissant_x = x0;
            croissant_y = y0 + randPoint2;
            croissant_speed = speed0 + ((rand() % 90));
        }

        // Движение корзины
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i mp = sf::Mouse::getPosition(window);
            cart_x = mp.x;
            cart_y = mp.y;
            cart->Move(cart_x, cart_y);
        }
        
        // Проверка касания и счет
        float point_x1 = cart_x - 50, point_x2 = cart_x + 30, point_y1 = cart_y - 50,point_y2 = cart_y + 10;

        if (((point_y1 <= milk_y) && (milk_y <= point_y2)) && ((point_x1 <= milk_x) && (milk_x <= point_x2))) 
        {
            milk_t = 0;
            randPoint1 = ((rand() % 2) * -130);
            milk_x = x0;
            milk_y = y0 + randPoint1;
            milk_speed = speed0 + ((rand() % 90));
            score++;
        }

        if (((point_y1 <= croissant_y) && (croissant_y <= point_y2)) && ((point_x1 <= croissant_x) && (croissant_x <= point_x2))) 
        {
            croissant_t = 0;
            if (randPoint1 == 0) randPoint2 = -130; else randPoint2 = 0;
            croissant_x = x0;
            croissant_y = y0 + randPoint2;
            croissant_speed = speed0 + ((rand() % 90));
            score++;
        }

        std::this_thread::sleep_for(40ms);
        
        window.clear();

        // Вывод фона, объектов
        window.draw(back);

        // Вывод счета
        text.setString(std::string("Score: ") + std::to_string(score));
        window.draw(text);

        // Вывод объектов
        window.draw(*milk->Get());
        window.draw(*croissant->Get());
        window.draw(*cart->Get());
        window.display();
    }

    if (cart != nullptr)
        delete cart;
    if (milk != nullptr)
        delete milk;
    if (croissant != nullptr)
        delete croissant;

    return 0;
}
