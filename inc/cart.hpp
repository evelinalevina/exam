#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace el
{
	

	class Cart
	{
	public:
		Cart(int x, int y);
		~Cart();

		bool Setup();
		void Move(float mx, float my);

		sf::Sprite* Get();
	private:
		int m_x, m_y, m_x0, m_y0;
		float m_mx, m_my;

		sf::Texture m_texture;
		sf::Sprite* m_cart = nullptr;
	};


}

