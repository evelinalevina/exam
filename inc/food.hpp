#pragma once
#include <SFML/Graphics.hpp>

namespace el
{
	const float g = 9.8; // /c^2

	class Food
	{
	public:
		Food(int x0, int y0, float angle, float speed);
		~Food();

		void Move(int x, int y);
		bool Setup(std::string foodImage);

		sf::Sprite* Get();
	private:
		
		int m_x, m_y, m_x0, m_y0;
		float m_angle, m_speed;
		std::string m_foodImage;
		sf::Texture m_texture;
		sf::Sprite* m_food = nullptr;
	};




}

